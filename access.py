from flask import  Flask,render_template,request

app = Flask(__name__)

@app.route('/')
def screen():
    return render_template('screen.html')


@app.route('/process', methods=['POST'])
def process():
# keyoard event
    event = request.form.get('text')
    print(event)
    return ""

if __name__== '__main__':
    app.debug = True
    app.run()


